## Competitive Programming problems

I along with @Ashish-012 and @ugtan decided to do atleast 1 problem a day on [Hackerrank](hackerrank.com/). So I thought it would be nice to add all this code in a repo as well.

__Our Hackerrank profiles__

* [ugtan](https://www.hackerrank.com/Ugtan)
* [mzfr](https://www.hackerrank.com/0xmzfr)
* [Ashish](https://www.hackerrank.com/Ashish_012)
